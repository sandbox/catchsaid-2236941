<?php

/**
 * Search API data alteration callback that filters out items based on their
 * language.
 */
class SearchApiIndexLocaleAlterCallback extends SearchApiAbstractAlterCallback {

  /**
   * Construct a data-alter callback.
   *
   * @param SearchApiIndex $index
   *   The index whose items will be altered.
   * @param array $options
   *   The callback options set for this index.
   */
  public function __construct(SearchApiIndex $index, array $options = array()) {
    $options += array(
      'language' => NULL,
    );
    parent::__construct($index, $options);
  }

  /**
   * Check whether this data-alter callback is applicable for a certain index.
   *
   * Only returns TRUE if the system is multilingual.
   *
   * @param SearchApiIndex $index
   *   The index to check for.
   *
   * @return boolean
   *   TRUE if the callback can run on the given index; FALSE otherwise.
   *
   * @see drupal_multilingual()
   */
  public function supportsIndex(SearchApiIndex $index) {
    return drupal_multilingual();
  }

  /**
   * Display a form for configuring this data alteration.
   *
   * @return array
   *   A form array for configuring this data alteration.
   */
  public function configurationForm() {
    $languages = language_list();
    $options = array();
    foreach ($languages as $language) {
      $options[$language->language] = $language->name;
    }
    $form['language'] = array(
      '#type' => 'radios',
      '#title' => t('Index language'),
      '#description' => t('This index should contain only items in this language.'),
      '#options' => $options,
      '#default_value' => isset($this->options['language']) ? $this->options['language'] : NULL,
    );
    return $form;
  }

  /**
   * Alter items before indexing.
   *
   * Items which are removed from the array won't be indexed, but will be marked
   * as clean for future indexing. This could for instance be used to implement
   * some sort of access filter for security purposes (e.g., don't index
   * unpublished nodes or comments).
   *
   * @param array $items
   *   An array of items to be altered, keyed by item IDs.
   */
  public function alterItems(array &$items) {
    foreach ($items as $i => &$item) {
        $handler = entity_translation_get_handler('node', $item);
        $translations = $handler->getTranslations()->data;
        if (!empty($translations) && (empty($translations[$this->options['language']]) || $translations[$this->options['language']]['status'] == 0)) {
          unset($items[$i]);
          continue;
        }
        elseif (empty($translations) && $item->language != $this->options['language']) {
          unset($items[$i]);
          continue;
        }
        $item->language = $this->options['language'];
    }
  }
}
