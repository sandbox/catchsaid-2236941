<?php

/**
 * Views query class using a Search API index as the data source.
 */
class SearchApiIndexLocaleViewsQuery extends SearchApiViewsQuery {
  /**
   * Create the basic query object and fill with default values.
   */
  public function init($base_table, $base_field, $options) {
    global $language_content;
    $indexes = variable_get('search_api_index_locale');
    $langcode = $language_content->language;
    if (!empty($indexes[$langcode])) {
      $base_table = 'search_api_index_' . strtolower(str_replace('-', '_', $indexes[$langcode]['index']));
    }
    parent::init($base_table, $base_field, $options);
  }
}
